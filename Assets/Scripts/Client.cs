﻿using System;
using System.Collections; 
using System.Collections.Generic; 
using System.Net; 
using System.Net.Sockets; 
using System.Text; 
using System.Threading; 
using UnityEngine; 

public class Client : MonoBehaviour
{	
	private TcpClient socketConnection; 	
	private Thread clientReceiveThread; 	
    [SerializeField] private int Port;

	public List<string> incomingMessageQueue;
	public List<string> outgoingMessageQueue;

	// Use this for initialization 	
	void Start () {
		ConnectToTcpServer();
		StartCoroutine(SendMessageQueue());     
	}  	

	/// <summary> 	
	/// Setup socket connection. 	
	/// </summary> 	
	private void ConnectToTcpServer () { 		
		try {  			
			clientReceiveThread = new Thread (new ThreadStart(ListenForData)); 			
			clientReceiveThread.IsBackground = true; 			
			clientReceiveThread.Start();  		
		} 		
		catch (Exception e) { 			
			Debug.Log("On client connect exception " + e);
			PopupMessage.Show("Couldn't connect to the server"); 		
		} 	
	}  	
	/// <summary> 	
	/// Runs in background clientReceiveThread; Listens for incomming data. 	
	/// </summary>     
	private void ListenForData() { 		
		try { 			
			socketConnection = new TcpClient("localhost", Port);  			
			Byte[] bytes = new Byte[1024];             
			while (true) { 				
				// Get a stream object for reading 				
				using (NetworkStream stream = socketConnection.GetStream()) { 					
					int length; 					
					// Read incomming stream into byte arrary. 					
					while ((length = stream.Read(bytes, 0, bytes.Length)) != 0) { 						
						var incommingData = new byte[length]; 						
						Array.Copy(bytes, 0, incommingData, 0, length); 						
						// Convert byte array to string message. 						
						string serverMessage = Encoding.ASCII.GetString(incommingData); 						
						Debug.Log("server message received as: " + serverMessage);
						incomingMessageQueue.Add(serverMessage); 					
					} 				
				} 			
			}         
		}         
		catch (SocketException socketException) {             
			Debug.Log("Socket exception: " + socketException);
			PopupMessage.Show(socketException.ToString());       
		}     
	}  	
	/// <summary> 	
	/// Send message to server using socket connection. 	
	/// </summary> 	
	public void CommitMessage(string msg) {
		outgoingMessageQueue.Add(msg);
	} 

	private IEnumerator SendMessageQueue(){
		while(true){
			foreach(var v in outgoingMessageQueue){
				sendMessage(v);
				//yield return new WaitForEndOfFrame();
			}
			outgoingMessageQueue.Clear();
			yield return new WaitForEndOfFrame();
		}
	}

	private void sendMessage(string msg){
		if (socketConnection == null) {             
			PopupMessage.Show("Couldn't send a message, connection to the server was not established");
			return;         
		}  		
		try {	 			
			// Get a stream object for writing. 			
			NetworkStream stream = socketConnection.GetStream(); 			
			if (stream.CanWrite) { 
				if(msg[msg.Length - 1] != '\n'){
					msg += '\n';
				}
				
				// Convert string message to byte array.                 
				byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(msg); 				
				// Write byte array to socketConnection stream.                 
				stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);                 
				Debug.Log($"Client sent message {msg}");             
			}         
		} 		
		catch (SocketException socketException) {             
			Debug.Log("Socket exception: " + socketException);         
		} 
	}

	private void OnDestroy() {
		try{
			clientReceiveThread.Abort();
		}catch(Exception e){
			PopupMessage.Show(e.ToString());
		}

		try{
			socketConnection.Close();
		}catch(Exception e){
			PopupMessage.Show(e.ToString());
		}
		
	}
}
