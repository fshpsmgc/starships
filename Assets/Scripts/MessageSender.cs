﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageSender : MonoBehaviour
{
    protected Client client;
    [SerializeField] float responceCheckTimer;
    [SerializeField] int maxResponceChecks;

    private void Awake() {
        //client = GameObject.Find("Client").GetComponent<Client>();    
    }

    protected IEnumerator WaitForResponse(int messageCount = 1, bool hidePopupMessage = false){
        int respondedMessagesCount = 0;
        int responceChecksCount = 0;
        while(responceChecksCount != maxResponceChecks){
            print($"{responceChecksCount}/{maxResponceChecks}");
            if(client.incomingMessageQueue.Count != 0){
                if(CheckResponce()){
                    respondedMessagesCount++;
                    if(respondedMessagesCount >= messageCount)
                        break;
                }
            }else{
                responceChecksCount++;
                yield return new WaitForSecondsRealtime(responceCheckTimer);
            }
        }

        if(hidePopupMessage){
           PopupMessage.Hide(); 
        }
    }

    virtual protected bool CheckResponce(){
        throw new System.NotImplementedException();
    }
}
