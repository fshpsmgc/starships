﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using TMPro;
using System.Windows;

public class LoginController : MonoBehaviour
{
    string[] worstPasswords = {
        "password",
        "123456",
        "12345678",
        "qwerty",
        "abc123",
        "monkey",
        "1234567",
        "letmein",
        "trustno1",
        "dragon",
        "baseball",
        "111111",
        "iloveyou",
        "master",
        "sunshine",
        "ashley",
        "bailey",
        "passw0rd",
        "shadow",
        "123123",
        "654321",
        "superman",
        "qazwsx",
        "michael",
        "Football",
        "welcome",
        "jesus",
        "ninja",
        "mustang",
        "password1",
        "123456789",
        "admin",
        "1234567890",
        "1234",
        "12345",
        "princess",
        "azerty",
        "0",
        "access",
        "696969",
        "batman",
        "1qaz2wsx",
        "login",
        "qwertyuiop",
        "solo",
        "starwars",
        "121212",
        "flower",
        "hottie",
        "loveme",
        "zaq1zaq1",
        "hello",
        "freedom",
        "whatever",
        "666666",
        "!@#$%^&*",
        "charlie",
        "aa123456",
        "donald",
        "qwerty123",
        "1q2w3e4r",
        "555555",
        "lovely",
        "7777777",
        "888888",
        "123qwe",
        "12345679",
        "987654321",
        "mynoob",
        "123321",
        "18atcskd2w",
        "3rjs1la7qe",
        "google",
        "1q2w3e4r5t",
        "zxcvbnm",
        "1q2w3e",
        "1111111",
        "123"
    };

    Client client;
    [SerializeField] float responceCheckTimer;
    [SerializeField] int maxResponceChecks;

    [SerializeField] InputField login;
    [SerializeField] InputField password;    
    [SerializeField] InputField repeatPassword;    
    [SerializeField] InputField publicName;    
    [SerializeField] Canvas authorizationCanvas;
    [SerializeField] Canvas gameCanvas;
    [SerializeField] TMP_Text passwordErrorText;
    [SerializeField] GameObject[] RegistrationForm;

    private void Awake() {
        client = GameObject.Find("Client").GetComponent<Client>();    
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SendLoginRequest(){
        client.CommitMessage($"login:{login.text},{password.text}");
        StartCoroutine(WaitForResponse());
    }

    public void SendRegistrationRequest(){
        client.CommitMessage($"register:{login.text},{password.text},{publicName.text}");
        StartCoroutine(WaitForResponse());
    }

    IEnumerator WaitForResponse(){
        int responceChecksCount = 0;
        while(responceChecksCount != maxResponceChecks){
            if(client.incomingMessageQueue.Count != 0){
                if(CheckResponce()){
                    break;
                }
            }else{
                responceChecksCount++;
                yield return new WaitForSecondsRealtime(responceCheckTimer);
            }
        }
    }

    bool CheckResponce(){
        foreach(var v in client.incomingMessageQueue){
            if(v.Contains("login ok")){
                print("login succsessful");
                client.incomingMessageQueue.Remove(v);
                authorizationCanvas.enabled = false;
                gameCanvas.enabled = true;

                GetComponent<PlayerInfo>().SetPlayerID(System.Convert.ToInt32(v.Split(':')[1]));
                return true;
            }

            if(v == "incorrect password"){
                print("incorrect password");
                client.incomingMessageQueue.Remove(v);
                return true;
            }

            if(v == "incorrect login"){
                print("incorrect login");
                client.incomingMessageQueue.Remove(v);
                return true;
            }
        }
        return false;
    }

    public void UI_CheckPassStrength(){
        if(CheckPasswordStrength()){
            password.image.color = Color.white;
        }else{
            password.image.color = Color.red;
        }
    }

    public void UI_InsertSecurePassword(){
        int passLength = 32;
        var pass = new byte[passLength];
        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        rng.GetNonZeroBytes(pass);
        
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < pass.Length; i++) {
            builder.Append(pass[i].ToString("x2"));
        }
        
        password.text = builder.ToString();
        repeatPassword.text = password.text;

        SetPasswordInputVisibility(InputField.InputType.Standard);
    }

    public void UI_TogglePasswordVisibility(){
        if(password.inputType == InputField.InputType.Standard){
            SetPasswordInputVisibility(InputField.InputType.Password);
        }else{
            SetPasswordInputVisibility(InputField.InputType.Standard);  
        }
    }

    //TODO
    public void UI_ToggleRegistrationForm(){
        foreach(var v in RegistrationForm){
            v.SetActive(!v.activeSelf);
        }
    }

    private void SetPasswordInputVisibility(InputField.InputType inputType){
        password.inputType = inputType;
        repeatPassword.inputType = inputType;
        
        password.ForceLabelUpdate();
        repeatPassword.ForceLabelUpdate();
    }

    public bool CheckPasswordStrength(){
        passwordErrorText.text = string.Empty;
        bool flag = true;
        if(password.text.Length < 6){
            passwordErrorText.text += "- the password is too short\n";
            flag = false; 
        }

        if(!Regex.IsMatch(password.text, @".*(?=.*([\d]+).*)(?=.*([a-z].*)).*")){
            passwordErrorText.text += "- must contain number and a letter\n";
            flag = false;
        }

        foreach(var v in worstPasswords){
            if(password.text == v){
                passwordErrorText.text += "- this password is too common\n";
                flag = false;
            }
        }
        return flag;
    }


}
