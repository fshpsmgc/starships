﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemButtonData{
    public int id;
    public string name;
    public string description;
    public string sprite_name;
    public int sell_cost;
}

public class ItemButton : MonoBehaviour
{
    [SerializeField] Image image;
    int item_id;
    string item_name;
    string item_description;
    int sell_cost;

    public void Set(string msg){
        ItemButtonData data = new ItemButtonData();
        string[] argv = msg.Split(',');
        data.id = System.Convert.ToInt32(argv[0]);
        data.name = argv[1];
        data.sprite_name = argv[2];
        data.sell_cost = System.Convert.ToInt32(argv[3]);
        data.description = argv[4];
        Set(data);
    }

    public void Set(ItemButtonData data){
        print("setting an item button");
        item_id = data.id;
        item_name = data.name;
        item_description = data.description;
        sell_cost = data.sell_cost;
        image.sprite = Resources.Load<Sprite>($"Sprites/Items/{data.sprite_name}");
    }

    public string GetDescription(){
        return $"<b>{item_name}</b>:{item_description}\n<b>Sell cost:</b>{sell_cost}";
    }
}
