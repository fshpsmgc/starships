﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public struct QuestPanelData{
    public int id;
    public string name;
    public string description;
    public int moneyReward;
    public int length;
    public int distance;
}

public class QuestPanelController : MonoBehaviour
{
    [SerializeField] TMP_Text questNameText;
    [SerializeField] TMP_Text questDescriptionText;
    [SerializeField] TMP_Text questDetailsText;

    int id;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Set(string msg){
        QuestPanelData data = new QuestPanelData();
        print(msg);
        string[] vs = msg.Split(',');
        data.name = vs[0];
        data.description = vs[1];
        data.moneyReward = System.Convert.ToInt32(vs[2]);
        data.length = System.Convert.ToInt32(vs[3]);
        data.distance = System.Convert.ToInt32(vs[4]);

        Set(data);
    }

    public void Set(QuestPanelData data){
        id = data.id;
        questNameText.text = data.name;
        questDescriptionText.text = data.description;
        questDetailsText.text = $"Reward: {data.moneyReward}\nLength: {data.length}\nDistance {data.distance}";
    }
}
