﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestMenuController : MessageSender
{
    [SerializeField] RectTransform questList;
    [SerializeField] GameObject questPanelPrefab;

    private void Awake() {
        client = GameObject.Find("Client").GetComponent<Client>();    
    }

    public void SendQuestInfoRequests(){
        PopupMessage.Show("Fetching available quests");
        ClearList();

        client.CommitMessage("req:quests");

        StartCoroutine(WaitForResponse(1, true));
    }

    override protected bool CheckResponce(){
        foreach(var v in client.incomingMessageQueue){
            if(v.Contains("quests:")){
                FillQuestList(v);
                client.incomingMessageQueue.Remove(v);
                return true;
            }
        }
        return false;
    }

    private void ClearList(){
        for(int i = 0; i < questList.childCount; i++){
            Destroy(questList.GetChild(i).gameObject);
        }
    }

    private void FillQuestList(string list){
        string[] quests = list.Split('\n');
        for(int i = 1; i < quests.Length - 1; i++){
            GameObject tmp = Instantiate(questPanelPrefab, questList);
            tmp.GetComponent<QuestPanelController>().Set(quests[i]);
            //tmp.GetComponentInChildren<TMPro.TMP_Text>().text = quests[i];
        }
    }

}
