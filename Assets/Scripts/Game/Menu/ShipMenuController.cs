﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipMenuController : MessageSender
{
    [SerializeField] Image engineImage;
    [SerializeField] Image hullImage;
    [SerializeField] Image cuImage;

    [SerializeField] RectTransform enginesList;
    [SerializeField] RectTransform hullsList;
    [SerializeField] RectTransform cusList;
    [SerializeField] GameObject shipModuleButtonPrefab;

    private void Awake() {
        client = GameObject.Find("Client").GetComponent<Client>();    
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SendShipInfoRequests(){
        PopupMessage.Show("Fetching ship modules information");
        ClearLists();

        client.CommitMessage("req:engines");
        client.CommitMessage("req:hulls");
        client.CommitMessage("req:cu");

        StartCoroutine(WaitForResponse(3, true));
    }

    override protected bool CheckResponce(){
        foreach(var v in client.incomingMessageQueue){
            if(v.Contains("engines_data")){
                FillEngineList(v);
                client.incomingMessageQueue.Remove(v);
                return true;
            }

            if(v.Contains("hulls_data")){
                FillHullsList(v);
                client.incomingMessageQueue.Remove(v);
                return true;
            }

            if(v.Contains("cu_data")){
                FillCUList(v);
                client.incomingMessageQueue.Remove(v);
                return true;
            }
        }
        return false;
    }

    private void ClearLists(){
        for(int i = 0; i < enginesList.childCount; i++){
            Destroy(enginesList.GetChild(i).gameObject);
        }

        for(int i = 0; i < hullsList.childCount; i++){
            Destroy(hullsList.GetChild(i).gameObject);
        }

        for(int i = 0; i < cusList.childCount; i++){
            Destroy(cusList.GetChild(i).gameObject);
        }
    }

    private void FillEngineList(string list){
        string[] engines = list.Split('\n');
        for(int i = 1; i < engines.Length - 1; i++){
            GameObject tmp = Instantiate(shipModuleButtonPrefab, enginesList);
            tmp.GetComponentInChildren<TMPro.TMP_Text>().text = engines[i];
        }
    }

    private void FillHullsList(string list){
        string[] hulls = list.Split('\n');
        for(int i = 1; i < hulls.Length - 1; i++){
            GameObject tmp = Instantiate(shipModuleButtonPrefab, hullsList);
            tmp.GetComponentInChildren<TMPro.TMP_Text>().text = hulls[i];
        }
    }

    private void FillCUList(string list){
        string[] cus = list.Split('\n');
        for(int i = 1; i < cus.Length - 1; i++){
            GameObject tmp = Instantiate(shipModuleButtonPrefab, cusList);
            tmp.GetComponentInChildren<TMPro.TMP_Text>().text = cus[i];
        }
    }
}
