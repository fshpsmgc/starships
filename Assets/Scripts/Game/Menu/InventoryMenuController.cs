﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class InventoryMenuController : MessageSender
{
    [SerializeField] RectTransform inventoryList;
    [SerializeField] TMP_Text descriptionText;
    [SerializeField] int selectedItem;
    [SerializeField] GameObject itemButtonPrefab;

    private void Awake() {
        client = GameObject.Find("Client").GetComponent<Client>();    
    }

    public void SendInventoryRequest(){
        PopupMessage.Show("Fetching inventory");
        ClearList();
        int pid = GameObject.Find("LoginManager").GetComponent<PlayerInfo>().GetPID();
        client.CommitMessage($"req:inventory:{pid}");

        StartCoroutine(WaitForResponse(1, true));
    }

    override protected bool CheckResponce(){
        foreach(var v in client.incomingMessageQueue){
            if(v.Contains("inventory:")){
                FillInventoryList(v);
                client.incomingMessageQueue.Remove(v);
                return true;
            }
        }
        return false;
    }

    private void ClearList(){
        for(int i = 0; i < inventoryList.childCount; i++){
            Destroy(inventoryList.GetChild(i).gameObject);
        }
    }

    private void FillInventoryList(string list){
        string[] items = list.Split('\n');
        for(int i = 1; i < items.Length - 1; i++){
            GameObject tmp = Instantiate(itemButtonPrefab, inventoryList);
            tmp.GetComponent<ItemButton>().Set(items[i]);
            int tempIndex = i - 1;
            tmp.GetComponent<Button>().onClick.AddListener(() => { UI_SelectItem(tempIndex); });
        }
    }

    public void UI_SelectItem(int id){
        inventoryList.GetChild(selectedItem).GetComponent<Button>().interactable = true;
        selectedItem = id;
        ItemButton itemButton = inventoryList.GetChild(id).GetComponent<ItemButton>();
        itemButton.GetComponent<Button>().interactable = false;
        descriptionText.text = itemButton.GetDescription();
    }

    public void UI_SellItem(){
        client.CommitMessage($"sell:{PlayerInfo.GetPlayerID()},{selectedItem}");
    }
}
