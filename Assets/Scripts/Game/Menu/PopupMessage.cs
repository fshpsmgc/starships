﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public enum PopupMessageType{
    Nothing,
    Ok,
    Cancel
}

public class PopupMessage : MonoBehaviour
{
    public static void Show(string message, PopupMessageType msgType = PopupMessageType.Ok){
        GameObject.Find("PopupCanvas").GetComponent<Canvas>().enabled = true;
        TMP_Text text = GameObject.Find("PopupText").GetComponent<TMP_Text>();
        text.text = message;
    }

    public static void Hide(){
        GameObject.Find("PopupCanvas").GetComponent<Canvas>().enabled = false;
    }

    public void UI_OKButton(){
        Hide();
    }
}
