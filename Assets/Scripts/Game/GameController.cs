﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] List<GameObject> tabs;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UI_SetTab(int index) {
        for (int i = 0; i < tabs.Count; i++) {
            if (i == index) {
                tabs[i].SetActive(true);
            } else {
                tabs[i].SetActive(false);
            }
        }
    }

    public void UI_QuitGame(){
        Application.Quit();
    }
}
