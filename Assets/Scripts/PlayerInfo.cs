﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    [SerializeField] int playerID;

    public void SetPlayerID(int id){
        if(id < 0){
            throw new System.Exception("Player ID cannot be negative");
        }
        playerID = id;
    }

    public int GetPID(){
        return playerID;
    }

    public static int GetPlayerID(){
        return GameObject.Find("LoginManager").GetComponent<PlayerInfo>().GetPID();
    }
}
