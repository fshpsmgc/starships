# Details

Date : 2020-03-12 00:51:28

Directory d:\Dev\Games\starships\Assets\Scripts

Total : 3 files,  287 codes, 21 comments, 35 blanks, all 343 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [Client.cs](/Client.cs) | C# | 74 | 17 | 4 | 95 |
| [Game/GameController.cs](/Game/GameController.cs) | C# | 26 | 2 | 8 | 36 |
| [Login/LoginController.cs](/Login/LoginController.cs) | C# | 187 | 2 | 23 | 212 |

[summary](results.md)