# Summary

Date : 2020-03-12 00:51:28

Directory d:\Dev\Games\starships\Assets\Scripts

Total : 3 files,  287 codes, 21 comments, 35 blanks, all 343 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 3 | 287 | 21 | 35 | 343 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 3 | 287 | 21 | 35 | 343 |
| Game | 1 | 26 | 2 | 8 | 36 |
| Login | 1 | 187 | 2 | 23 | 212 |

[details](details.md)